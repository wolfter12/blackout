import * as dotenv from "dotenv";
dotenv.config();
import ping from "ping";
import TelegramBot from "node-telegram-bot-api";
import { CronJob } from "cron";
import config from "./config";

console.log("🚀 Server is running");

let lastDateTimeElectricity: Date | undefined;
let lastDateTimeElectricityIsMissing: Date | undefined;
let isBlackout: boolean | undefined;

const main = async () => {
  const result = await ping.promise.probe(config.ROUTER_DOMAIN, {
    timeout: 10,
    extra: ["-i", "2"],
  });

  isBlackout = !result.alive;

  if (result.alive) {
    lastDateTimeElectricity = new Date();
  } else {
    lastDateTimeElectricityIsMissing = new Date();
  }
};

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(config.BOT_TOKEN, {
  polling: true,
});

// Listen for any kind of message. There are different kinds of messages.
bot.on("message", (msg) => {
  const chatId = msg.chat.id;
  const text = msg.text;

  if (text === "/status") {
    bot.sendMessage(
      chatId,
      isBlackout ? "Електроенергії немає" : "Електроенергія є"
    );
  }

  if (text === "/lastOn") {
    bot.sendMessage(
      chatId,
      `Останнього разу електроенергія була ${
        lastDateTimeElectricity
          ? lastDateTimeElectricity.toDateString() +
            " " +
            lastDateTimeElectricity.toTimeString()
          : "невідомо коли"
      }`
    );
  }

  if (text === "/lastOff") {
    bot.sendMessage(
      chatId,
      `Останнього разу електроенергії не було ${
        lastDateTimeElectricityIsMissing
          ? lastDateTimeElectricityIsMissing.toDateString() +
            " " +
            lastDateTimeElectricityIsMissing.toTimeString()
          : "невідомо коли"
      }`
    );
  }
});

try {
  main();
} catch (error) {
  console.log(error);
}

const job = new CronJob("* * * * *", async (): Promise<void> => {
  console.log("Cron is running", new Date().toISOString());
  try {
    main();
  } catch (error) {
    console.log(error);
  }
});

job.start();
