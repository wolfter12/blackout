const config = {
  ROUTER_DOMAIN: process.env.ROUTER_DOMAIN || "ROUTER_DOMAIN_PLACEHOLDER",
  BOT_TOKEN: process.env.BOT_TOKEN || "BOT_TOKEN_PLACEHOLDER",
};

export default config;
